<?php

use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
        'user_name' => $faker->text(50),
        'is_activated' => $faker->boolean,
        'email' => $faker->text(100),
        'jobs' => $faker->text(100),
        'experience' => $faker->text(100),
        'freelancer' => $faker->text(100),
    ];
});
