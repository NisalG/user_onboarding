<?php

use Faker\Generator as Faker;

$factory->define(App\OnboardingPercentage::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomDigitNotNull,
        'onboarding_perentage' => $faker->randomDigitNotNull,
        'count_applications' => $faker->randomDigitNotNull,
        'count_accepted_applications' => $faker->randomDigitNotNull,
    ];
});
