<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoutesTests extends TestCase
{
    /**
     * Test Home page loading
     *
     * @return void
     */
    public function testLoadHomePage()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
