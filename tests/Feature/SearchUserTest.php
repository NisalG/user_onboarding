<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchUserTest extends TestCase
{
    /**
     * Test User search feature response status and format
     *
     * @return void
     */
    public function testUserSearch()
    {
        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('GET', 'profiles/search');

        $response
            ->assertStatus(200)
            ->assertJson([
            ]);
    }
}
