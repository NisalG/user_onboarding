<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\OnboardingPercentage;

class OnboardingStatusDataTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if data returns in proper JSON format
     *
     * @return void
     */
    public function testOnboardingStatusChartData()
    {
        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('GET', 'onboading/get_percentages');

        $response
            ->assertStatus(200)
            ->assertJson([
            ]);
    }

    /**
     * Test if user can update status data
     *
     * @return void
     */
    public function testUserCanUpdateStatus()
    {
        factory(OnboardingPercentage::class)->create();

        $onboardingPercentages = OnboardingPercentage::all();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection',$onboardingPercentages);
        $this->assertGreaterThanOrEqual(1, count($onboardingPercentages));
    }
}
