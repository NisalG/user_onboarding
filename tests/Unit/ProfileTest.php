<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Profile;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if data returns
     *
     * @return void
     */
    public function testGetProfilesArray()
    {
        factory(Profile::class)->create();

        $profiles = Profile::all();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection',$profiles);
        $this->assertGreaterThanOrEqual(1, count($profiles));
    }

    /**
     * Test if can save data
     *
     * @return void
     */
    function testCanSaveProfiles()
    {
        factory(Profile::class, 2)->create();

        $profiles = Profile::all();

        $this->assertEquals(2, $profiles->count());
    }
}
