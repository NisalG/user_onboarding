<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\OnboardingPercentage;

class OnboardingPercentageTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if data returns
     *
     * @return void
     */
    public function testGetOnboardingPercentagesArray()
    {
        factory(OnboardingPercentage::class)->create();

        $onboardingPercentages = OnboardingPercentage::all();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection',$onboardingPercentages);
        $this->assertGreaterThanOrEqual(1, count($onboardingPercentages));
    }

    /**
     * Test if can save data
     *
     * @return void
     */
    function testCanSaveOnboardingPercentages()
    {
        factory(OnboardingPercentage::class, 2)->create();

        $onboardingPercentages = OnboardingPercentage::all();

        $this->assertEquals(2, $onboardingPercentages->count());
    }

}
