# Laravel + Vue.js Tech Test - Sample application for user onboarding: user_onboarding

This repo contains the sample application code for an user onboarding process with steps which was developed in Laravel and Vue.js

## Setup Instructions

-  Clone into a HTTP server(E.g.: Apache) file system
-  Create a MySQL database
-  Set appropriate database connection settings in .env file
-  Run database migrations by: `$ php artisan migrate`
-  Install PHP packages by: `$ composer install`
-  Install JS dependencies by: `$ npm install`
-  Run the test suite by: `$ vendor/bin/phpunit`
-  Serve the application using: `$ php artisan serve`
	