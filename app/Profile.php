<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    // protected $table='profiles';
    protected $fillable = [
        'id', 'user_name','is_activated','email','jobs','experience','freelancer'
    ];

    /**
     * Create One to One Relationship of Profile and Percentage Models
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function onboarding()
    {
        return $this->hasOne('App\OnboardingPercentage','user_id','id');
    }
}
