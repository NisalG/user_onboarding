<?php

namespace App\Http\Controllers;
use App\Profile;
use App\OnboardingPercentage;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Profile controller constructor
     *
     */
    public function __construct()
    {
//        $this->middleware('admin_auth');
//        $this->middleware('auth');

        // Links
        $this->url_level1 = '/profiles';

        // Model and Module name
        $this->module = "Profile";
        $this->modelObj = new Profile();

        // Module Message
        $this->savedMsg = $this->module . " has been added successfully!";
        $this->updateMsg = $this->module . " has been updated successfully!";
        $this->deleteMsg = $this->module . " has been deleted successfully!";
        $this->deleteErrorMsg = $this->module . " can not deleted!";
        $this->saveErrorMsg = $this->module . " not saved!";
    }

    /**
     * Show the index
     *
     * @return array of objects
     */
    public function index()
    {
        $entities = $this->modelObj->with('onboarding')->get();
        $new_id = $this->modelObj->max('id') + 1;
        return compact('entities','new_id');
    }

    /**
     * Create profile and percentage
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        //validate
        $this->validate($request,['id'=>'required|unique:profiles|numeric','user_name'=>'required|unique:profiles|string|max:50',]);

        try{
            DB::beginTransaction();

            //create the profile
            $obj = $this->modelObj->create($request->all());
            $obj->id = $request->id;

            //Create percentage status
            $percentage = new OnboardingPercentage();
            $percentage->user_id = $request->id;
            $percentage->onboarding_perentage = 0;
            $percentage->count_applications = 0;
            $percentage->count_accepted_applications = 0;
            $percentage->save();

            DB::commit();
            return  $this->savedMsg;
        }
        catch(Exception $ex){
            DB::rollBack();
            return $this->saveErrorMsg;
        }
    }

    /**
     * Search user by user name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchUser(Request $request)
    {
        $obj = $this->modelObj->where('user_name',$request->user_name)->with('onboarding')->first();
        return response()->json($obj);
    }

    /**
     * Update profile and percentage
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();

            //validate
            $this->validate($request,['id'=>'required|numeric','user_name'=>'required|string|max:50','jobs'=>'max:100']);

            $obj = $this->modelObj->findOrFail($id);
            $obj->is_activated = $request['is_activated'];
            $obj->email = $request['email'];
            $obj->jobs = $request['jobs'];
            $obj->experience = $request['experience'];
            $obj->freelancer = $request['freelancer'];
            $obj->save();

            $percentage = OnboardingPercentage::where('user_id',$id)->first();
            $percentage->onboarding_perentage = $request['onboarding']['onboarding_perentage'];
            $percentage->save();

            DB::commit();
            return  $this->updateMsg;
        }
        catch(Exception $ex){
            DB::rollBack();
            return $this->saveErrorMsg;
        }
    }

    /**
     * Generate JSON formatted data to render chart
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOnboardingPercentage()
    {
        $name = [];
        $data = [];

        $response = OnboardingPercentage::where('onboarding_perentage',0)->get();
        array_push($data,count($response));
        array_push($name,'1. Create account');

        $response = OnboardingPercentage::where('onboarding_perentage',20)->get();
        array_push($data,count($response));
        array_push($name,'2. Activate account ');

        $response = OnboardingPercentage::where('onboarding_perentage',40)->get();
        array_push($data,count($response));
        array_push($name,'3. Provide profile information ');

        $response = OnboardingPercentage::where('onboarding_perentage',50)->get();
        array_push($data,count($response));
        array_push($name,'5. Do you have relevant experience in these jobs');

        $response = OnboardingPercentage::where('onboarding_perentage',70)->get();
        array_push($data,count($response));
        array_push($name,'4. What jobs are you interested in');

        $response = OnboardingPercentage::where('onboarding_perentage',90)->get();
        array_push($data,count($response));
        array_push($name,'6. Are you freelancer');

        $response = OnboardingPercentage::where('onboarding_perentage',99)->get();
        array_push($data,count($response));
        array_push($name,'7. Waiting for approval ');

        $response = OnboardingPercentage::where('onboarding_perentage',100)->get();
        array_push($data,count($response));
        array_push($name,'8. Approval');

        return response()->json(compact('data','name'));
    }
}
