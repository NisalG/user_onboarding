<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnboardingPercentage extends Model
{
    protected $table='percentages';

    protected $fillable = [
        'user_id', 'onboarding_perentage', 'count_applications', 'count_accepted_applications'
    ];
}
