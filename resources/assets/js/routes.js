import VueRouter from 'vue-router'

let routes = [
  {
    path: '/',
    component: require('./components/User.vue')
  },
  {
    path: '/admin',
    component: require('./components/Admin.vue')
  }
]

export default new VueRouter({
  routes
})
