<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Onboading</title>

    <link rel="stylesheet" href="/css/app.css">

    <style>
    </style>
</head>
<body>
    <div class="container-fluid" id="app">
        <nav class="navbar navbar-default">
            <div class="container">
                <ul class="nav navbar-nav">
                    <router-link tag="li" to="/" exact>
                        <a>User</a>
                    </router-link>
                    <router-link tag="li" to="/admin" exact>
                        <a>Admin</a>
                    </router-link>
                </ul>
            </div>
        </nav>

        <router-view></router-view>
    </div>
    <script src="/js/app.js"></script>
</body>
</html>
